<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class GamesController extends Controller
{
    public function index(){
        $reads = DB::table('games')->get();
                
        return view('game.index',compact('reads'));
    }

    public function create(){
        return view('game.create');
    }

    public function show($id){
        $details= DB::table('games')->where('id',$id)->first();

        return view('games.show',compact('details'));
    }

    public function edit($id){
        $details= DB::table('games')->where('id',$id)->first();

        return view('game.edit',compact('details'));
    }

    public function store(Request $request){

        $request -> validate([
            "name" => 'required',
            "gameplay" => 'required',
            "developer" => 'required',
            "year" => 'required'
        ]);

        $query = DB::table('games')->insert([
            "name" => $request["name"],
            "gameplay" => $request["gameplay"],
            "developer" => $request["developer"],
            "year" => $request["year"]
        ]);

        return redirect('/game')->with('succes','Post berhasil di simpan !');
    }

    public function update($id, Request $request){

        $request -> validate([
            "name" => 'required',
            "gameplay" => 'required',
            "developer" => 'required',
            "year" => 'required'
        ]);

        $query = DB::table('games')->where('id',$id)->update([
            "name" => $request["name"],
            "gameplay" => $request["gameplay"],
            "developer" => $request["developer"],
            "year" => $request["year"]
        ]);

        return redirect('/game')->with('succes','Update games berhasil di simpan !');
    }

    public function destroy($id,Request $request){
            $query = DB::table('games')->where('id',$id)->delete();

            return redirect('/game')->with('delete',"games dengan ID: $id berhasil di hapus !");
    }
}
