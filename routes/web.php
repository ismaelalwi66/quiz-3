<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('game/erd');
});

Route::get('/game', 'GamesController@index');

Route::get('/game/create', 'GamesController@create');

Route::get('/game/{game_id}','GamesController@show');

Route::get('/game/{game_id}/edit','GamesController@edit');

Route::post('/game','GamesController@store');

Route::put('/game/{game_id}','GamesController@update');

Route::delete('/game/{game_id}','GamesController@destroy');
