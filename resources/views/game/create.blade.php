@extends('layouts.master')

@section('content')
<section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>Games</h1>
        </div>
      </div>
    </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">

    <!-- Default box -->
    <div class="card">
      <div class="card-header">
        <h3 class="card-title">Create New Game</h3>

        <div class="card-tools">
          <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
            <i class="fas fa-minus"></i>
          </button>
          <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
            <i class="fas fa-times"></i>
          </button>
        </div>
      </div>
      <div class="card-body">

        <form action="/game" method="POST">
            @csrf
            <div class="card-body">
            <div class="form-group">
                <label for="nama">Nama</label>
                <input name="name" type="text" class="form-control" id="" placeholder="Nama Game" value="{{old('name','')}}">
                @error('name')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>
            <div class="form-group">
                <label for="gameplay">Gameplay</label>
                <input name="gameplay" type="text" class="form-control" id="" placeholder="Gameplay" value="{{old('gameplay','')}}">
                @error('gameplay')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>
            <div class="form-group">
                <label for="developer">Developer</label>
                <input name="developer" type="text" class="form-control" id="" placeholder="Developer" value="{{old('developer','')}}">
                @error('developer')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>
            <div class="form-group">
                <label for="year">Year</label>
                <input name="year" type="number" class="form-control" id="" placeholder="Year" value="{{old('year','')}}">
                @error('year')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>
            </div>
            <!-- /.card-body -->

            <div class="card-footer">
            <button type="submit" class="btn btn-primary">Create</button>
            </div>
        </form>
        
    </div>
    <!-- /.card-body -->
    <!-- /.card-footer-->
  </div>
  <!-- /.card -->

</section>
@endsection