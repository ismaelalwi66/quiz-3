@extends('layouts/master')

@section('content')
    
<section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>Cast</h1>
        </div>
      </div>
    </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">

    <!-- Default box -->
    <div class="card">
      <div class="card-header">
        <h3 class="card-title">Edit Game : <b>{{$details->id}} </b></h3>

        <div class="card-tools">
          <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
            <i class="fas fa-minus"></i>
          </button>
          <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
            <i class="fas fa-times"></i>
          </button>
        </div>
      </div>
      <div class="card-body">

        <form action="/game/{{$details->id}}" method="POST">
            @csrf
            @method('PUT')
            <div class="card-body">
            <div class="form-group">
                <label for="nama">Nama</label>
                <input name="name" type="text" class="form-control" id="" placeholder="Nama Game" value="{{old('nama',$details->name)}}">
                @error('name')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>
            <div class="form-group">
                <label for="gameplay">Gameplay</label>
                <input name="gameplay" type="text" class="form-control" id="" placeholder="Gameplay" value="{{old('nama',$details->gameplay)}}">
                @error('gameplay')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>
            <div class="form-group">
                <label for="developer">Developer</label>
                <input name="developer" type="text" class="form-control" id="" placeholder="Developer" value="{{old('nama',$details->developer)}}">
                @error('developer')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>
            <div class="form-group">
                <label for="year">Year</label>
                <input name="year" type="number" class="form-control" id="" placeholder="Year" value="{{old('nama',$details->year)}}">
                @error('year')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>
            </div>
            <!-- /.card-body -->

            <div class="card-footer">
            <button type="submit" class="btn btn-primary">Edit</button>
            </div>
        </form>
        
    </div>
    <!-- /.card-body -->
    <!-- /.card-footer-->
  </div>
  <!-- /.card -->

</section>

@endsection