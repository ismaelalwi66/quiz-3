@extends('layouts.master')

@section('content')
    
<section class="content">

    <!-- Default box -->
    <div class="card">
    <div class="card-header">
        <h3 class="card-title">Game List</h3>

        <div class="card-tools">
        <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
            <i class="fas fa-minus"></i>
        </button>
        <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
            <i class="fas fa-times"></i>
        </button>
        </div>
    </div>
    <div class="card-body">
        @if(session('succes'))
        <div class="alert alert-success">
            {{session('succes')}}
        </div>
        @endif
        @if(session('delete'))
        <div class="alert alert-danger">
            {{session('delete')}}
        </div>
        @endif
        <table class="table table-striped">
            <thead>
              <tr>
                <th style="width: 10px">#</th>
                <th>Nama Game</th>
                <th>Gameplay</th>
                <th>Developer</th>
                <th>Year</th>
                <th colspan="3" style="text-align: center">Action</th>
              </tr>
            </thead>
            <tbody>
                @forelse ($reads as $key => $val) {{-- mengambil data array dari controller dgn funsi (commpact reads) kemudian d buat variabel key yg berisi array assosiatif kemudian diambil lagi isi nya dgn variabel val --}}
                    <tr>
                        <td> {{ $key+1 }} </td>
                        <td> {{$val->name}} </td>
                        <td> {{$val->gameplay}} </td>
                        <td> {{$val->developer}} </td>
                        <td> {{$val->year}} </td>
                        <td style="width: 20px">
                            <a href="/game/{{$val->id}}" class="btn btn-info btn-sm">Detail</a>
                        </td>
                        <td style="width: 20px;">
                            <a href="/game/{{$val->id}}/edit" class="btn btn-default btn-sm">Edit</a>
                        </td>
                        <td style="width: 20px;">
                            <form action="/game/{{$val->id}}" method="POST">
                                @csrf
                                @method('DELETE')
                                <input type="submit" value="Delete" class="btn btn-danger btn-sm">
                            </form>
                        </td>
                    </tr>
                    @empty
                    <tr>
                        <td colspan="7" align="center">No Post</td>
                    </tr>
                
                @endforelse              
            </tbody>
          </table>

          
    </div>
    <div class="card-footer">
        <a class="btn btn-primary" href="/game/create">Create New Game</a>
    </div>
    </div>
<!-- /.card -->

</section>


@endsection

@push('scripts')
<script>
    Swal.fire({
        title: "Berhasil!",
        text: "Memasangkan script sweet alert",
        icon: "success",
        confirmButtonText: "Cool",
    });

    Swal.fire()

</script>

@endpush